'''
Code adopted from the following paper:

Z. Mao, N. Chimitt, and S. H. Chan, "Accerlerating Atmospheric Turbulence
Simulation via Learned Phase-to-Space Transform", ICCV 2021

Arxiv: https://arxiv.org/abs/2107.11627

Zhiyuan Mao, Nicholas Chimitt, and Stanley H. Chan
Copyright 2021
Purdue University, West Lafayette, IN, USA


Ripon - I'm Using demo.py to generate the Turbulence Images using same strength.

'''



from VASTsimulator.simulator import Simulator
import matplotlib.pyplot as plt
import torch
import numpy as np


class SimWrapper:
    def __init__(self, data_path, device = torch.device('cuda:0'), dtype=torch.float32, str_list=(1,2,3,4)):
        self.device = device
        self.dtype = dtype
        self.data_path = data_path
        self.simulator_array = [Simulator(strength, 112, data_path=self.data_path, device=self.device, dtype=self.dtype) for strength in str_list]
                                
        for sim in self.simulator_array:
            sim.eval()
        

    def simulate(self, input_img):
        # expects numpy.ndarray of shape (112,112,3)
        out_img_list = []
        input_img = torch.from_numpy(input_img).to(self.device, self.dtype).permute(2, 0, 1)
        for simulator in self.simulator_array:
            img = simulator(input_img).permute(1, 2, 0).cpu().numpy()
            img = np.clip(img, 0, 255)
            out_img_list.append(img)
        self.img_list = out_img_list
        return out_img_list
    
    def save(self):
        for i,img in zip((1,2,3,4), self.img_list):
            plt.imsave(f'sample_{i}.jpg', img/255)
